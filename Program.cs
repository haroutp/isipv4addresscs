﻿using System;
using System.Text.RegularExpressions;

namespace IsIPv4Address
{
    class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine(isIPv4Address(".254.255.0"));
            System.Console.WriteLine(isIPv4Address("172.316.254.1"));
            System.Console.WriteLine(isIPv4Address("172.16.254.1"));
            System.Console.WriteLine(isIPv4Address("1.1.1.1a"));
            System.Console.WriteLine(isIPv4Address("1"));
            System.Console.WriteLine(isIPv4Address("0.254.255.0"));
            System.Console.WriteLine(isIPv4Address("1.23.256.255."));
            System.Console.WriteLine(isIPv4Address("1.23.256.."));
            System.Console.WriteLine(isIPv4Address("0..1.0"));
            System.Console.WriteLine(isIPv4Address("35..36.9.9.0"));
            System.Console.WriteLine(isIPv4Address("1.1.1.1.1"));
            System.Console.WriteLine(isIPv4Address("1.256.1.1"));
            System.Console.WriteLine(isIPv4Address("a0.1.1.1"));
            System.Console.WriteLine(isIPv4Address("7283728"));
            System.Console.WriteLine(isIPv4Address("255.255.255.255abcdekjhf"));



        }
        static bool isIPv4Address(string inputString) {
            string[] arr = inputString.Split('.');
            if(arr.Length < 4 || arr.Length > 4){
                return false;
            }
            
            foreach(string item in arr){
                bool check = Regex.IsMatch(item, "^[0-9]{1,254}$");
                if(!check){
                        return false;
                    }
                if(String.IsNullOrEmpty(item) || string.IsNullOrWhiteSpace(item)){
                    return false;
                }
                if(Convert.ToInt64(item) > 255 || Convert.ToInt64(item) < 0){
                    return false;
                }
            }
            return true;
        }

    }
}
